-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 22, 2022 at 05:11 AM
-- Server version: 8.0.17
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bhmsv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `autoreport`
--

CREATE TABLE `autoreport` (
  `projectID` int(11) NOT NULL,
  `frontCover` int(11) NOT NULL,
  `info` int(11) NOT NULL,
  `overviewchart` int(11) NOT NULL,
  `chart` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `conclusion` int(11) NOT NULL,
  `backCover` int(11) NOT NULL,
  `daily` int(11) NOT NULL DEFAULT '0',
  `monthly` int(11) NOT NULL DEFAULT '0',
  `weekly` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `autoreport`
--

INSERT INTO `autoreport` (`projectID`, `frontCover`, `info`, `overviewchart`, `chart`, `event`, `conclusion`, `backCover`, `daily`, `monthly`, `weekly`) VALUES
(6, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `axis`
--

CREATE TABLE `axis` (
  `id` int(11) NOT NULL,
  `sensorid` int(11) NOT NULL,
  `axis` text NOT NULL,
  `unit` text NOT NULL,
  `factor` double NOT NULL,
  `minLimit` double DEFAULT NULL,
  `maxLimit` double DEFAULT NULL,
  `columnRead` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `projectID` int(11) NOT NULL,
  `email` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `projectID`, `email`, `status`) VALUES
(1, 6, 'pakkawat.kh@gmail.com', 1),
(2, 6, 'art@gmail.com', 1),
(3, 6, 'may@gmail.com', 1),
(4, 6, 'kate@gmail.com', 1),
(9, 6, 'e@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `onscreenreport`
--

CREATE TABLE `onscreenreport` (
  `projectID` int(11) NOT NULL,
  `frontCover` int(11) NOT NULL,
  `info` int(11) NOT NULL,
  `overviewchart` int(11) NOT NULL,
  `chart` int(11) NOT NULL,
  `event` int(11) NOT NULL,
  `conclusion` int(11) NOT NULL,
  `backCover` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `onscreenreport`
--

INSERT INTO `onscreenreport` (`projectID`, `frontCover`, `info`, `overviewchart`, `chart`, `event`, `conclusion`, `backCover`) VALUES
(6, 0, 1, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `picID` int(11) NOT NULL,
  `orderID` int(11) NOT NULL,
  `label` text NOT NULL,
  `fileName` text NOT NULL,
  `projectID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`picID`, `orderID`, `label`, `fileName`, `projectID`) VALUES
(7, 200, 'Cross section', 'PolicyViz_WorldTGM-1140x700.png', 3),
(11, 100, 'Map', '04.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `projectID` int(11) NOT NULL,
  `projectName` text NOT NULL,
  `isPassword` tinyint(4) NOT NULL,
  `password` text NOT NULL,
  `shortenURL` text NOT NULL,
  `address` text NOT NULL,
  `dateTime` text NOT NULL,
  `duration` int(11) NOT NULL,
  `linenumber` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`projectID`, `projectName`, `isPassword`, `password`, `shortenURL`, `address`, `dateTime`, `duration`, `linenumber`, `status`) VALUES
(3, 'Param 5 bridge', 1, '1234567', 'param5', 'Param 5 bridge, Nontraburi', '2022-09-03 12:00', 3000, 12, 1),
(4, 'Param 8 bridge', 0, '', 'param8', 'param 8 bridge, Nontraburi', '2022-09-09 1:00', 5000, 12, 1),
(5, 'Param 9 Bridge', 1, '1234578', 'Param9', 'Param 9 Bridge, Bangkok', '2022-09-05 12:00', 5000, 17, 1),
(6, 'Param 11 bridge', 0, '', 'Param11', 'param 11 road,  Bangkok', '2022-09-01 12:00', 3000, 20, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `projectID` int(11) NOT NULL,
  `onScreenReport` int(11) NOT NULL,
  `autoReport` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id`, `projectID`, `onScreenReport`, `autoReport`) VALUES
(17, 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sensor`
--

CREATE TABLE `sensor` (
  `id` int(11) NOT NULL,
  `projectID` int(11) NOT NULL,
  `sensorName` text NOT NULL,
  `sensorType` int(11) NOT NULL,
  `picture` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci,
  `online` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `sensor`
--

INSERT INTO `sensor` (`id`, `projectID`, `sensorName`, `sensorType`, `picture`, `online`) VALUES
(9, 6, 'Acc#1', 1, NULL, 0),
(10, 6, 'Acc#2', 1, NULL, 0),
(11, 6, 'Strain#1', 2, NULL, 0),
(12, 6, 'LVDT#1', 4, NULL, 0),
(13, 6, 'tilt#1', 6, NULL, 0),
(14, 6, 'Strain#2', 2, NULL, 0),
(15, 6, 'Tile2', 6, NULL, 0),
(16, 6, 'Acc#3', 1, NULL, 0),
(17, 6, 'Acc#4', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sensortype`
--

CREATE TABLE `sensortype` (
  `id` int(11) NOT NULL,
  `typeName` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `sensortype`
--

INSERT INTO `sensortype` (`id`, `typeName`) VALUES
(1, 'Accelerometer'),
(2, 'Strain gauge'),
(4, 'Displacement sensor'),
(6, 'Tilt sensor');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `adminID` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `hashkey` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`adminID`, `username`, `password`, `hashkey`) VALUES
(1, 'admin', '1234', '@(gcvemn^4NAzaCT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autoreport`
--
ALTER TABLE `autoreport`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `axis`
--
ALTER TABLE `axis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `onscreenreport`
--
ALTER TABLE `onscreenreport`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`picID`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`projectID`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensor`
--
ALTER TABLE `sensor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sensortype`
--
ALTER TABLE `sensortype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`adminID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `axis`
--
ALTER TABLE `axis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `picID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `projectID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sensor`
--
ALTER TABLE `sensor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sensortype`
--
ALTER TABLE `sensortype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `adminID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
